<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <script
            src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>

        <title>Projecto Calculadora</title>
    </head>

    <body>


        <form method="post">
            <p> 
            </p>
            <p>
                <input class="TextField"type="text" id="expressao" /> 
            </p>
            <p>
                <button type="button" value="1">1</button>
                <button type="button" value="2">2</button>
                <button type="button" value="3">3</button>
                <button type="button" value="*">*</button>
                <button type="button" value="/">/</button>
            </p>
            <p>
                <button type="button" value="4">4</button>
                <button type="button" value="5">5</button>
                <button type="button" value="6">6</button>
                <button type="button" value="+">+</button>
                <button type="button" value="-">-</button>
            </p>
            <p>
                <button type="button" value="7">7</button>
                <button type="button" value="8">8</button>
                <button type="button" value="9">9</button>
                <button type="button" value="0">0</button>
                <button type="button" value="=" class="igual">=</button>
            </p>
        </form>

    </body>
</html>

<script>
    $(function () {

        var expressao = $("#expressao");
// Quando clicado em algum botão da calculadora (Exceto o de igual e limpar)
        $("button:not(.igual, .clear)").click(function () {
            // Coloca o valor na área de calculos e da foco no campo
            if (expressao.val().indexOf("=") != -1) {
                expressao.val('');
            }

            expressao.val(expressao.val() + $(this).val());
            expressao.focus();
        });
// Quando enviado expressão
        $(".igual").click(function () {
            // Se a expressão não estiver vazia
            if (expressao.val() != "") {
                // Envia para o processamento
                $.post("calcular.php", {expressao: expressao.val()}, function (resultado) {
                    // Quando retorna, exibe o resultado
                    var html = expressao.val() + " = " + resultado;
                    expressao.val(html);
                });
            }
        });
    });
</script>